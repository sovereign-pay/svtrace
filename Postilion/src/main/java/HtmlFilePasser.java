import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.*;

public class HtmlFilePasser {
    public static void main(String[] args)  throws IOException {
        //Original file
        File input = new File("./Postilion/src/main/java/MyTraceFile1.html");
        Document doc = Jsoup.parse(input, "UTF-8","");
        preGenerator(buildWithJava(input));
        var req = finalBuilderHtml(doc, preGenerator(buildWithJava(input)));
        File ft = new File("MyTraceFile.html");
        //Convert java Element to html file
        FileUtils.writeStringToFile(ft, req.outerHtml(), StandardCharsets.UTF_8);
        //Run file in the browser
        Desktop.getDesktop().browse(ft.toURI());

        //Timer to execute trace attachment to the file set to 15 seconds
        ScheduledExecutorService executor =
                Executors.newSingleThreadScheduledExecutor();
        Runnable periodicTask = new Runnable() {
            public void run() {
                preGenerator(buildWithJava(ft));
                var req = finalBuilderHtml(doc, preGenerator(buildWithJava(ft)));
                try {
                    FileUtils.writeStringToFile(ft, req.outerHtml(), StandardCharsets.UTF_8);
                } catch (IOException e) {
                    System.out.println("File not found");
                }
            }
        };
        executor.scheduleAtFixedRate(periodicTask, 0, 15, TimeUnit.SECONDS);
    }

    //Function to get the sample trace and clone
    static Element buildWithJava(File input){
        Document doc = null;
        try {
            doc = Jsoup.parse(input, "UTF-8","");
        } catch (IOException e) {
            e.printStackTrace();
        }
        var clone = doc.body().children().last().children().last().clone();
        return clone;
    }
    //Function to modify the trace and add a unique data
    static Element preGenerator(Element a){
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("MMMM , yyyy hh:mm:ss");
        String strDate= formatter.format(date);
        Element s = a.getElementsByClass("expandable").get(0);
        Element m = s.getElementsByTag("pre").get(1).html(stringGenerator());
        Random random = new Random();
        var h = new Double [15];
        String s1 = "data_";
        String b = "binary_";
        for (int i = 0; i <h.length ; i++) {
            h[i] = Math.floor(Math.random() * 100);
            s1 += h[i]+"";
            b += h[i] + "";
        }
        String att1 = "javascript:toggleVisibility('"+s1+"');";
        String att2 = "javascript:toggleVisibility('"+b+"');";
        s.getElementsByTag("a").get(0).attr("onclick", att1);
        s.getElementsByTag("a").get(1).attr("onclick",att2);
        s.getElementsByClass("message_data").get(0).id(s1);
        s.getElementsByClass("message_data").get(1).id(b);
        s.getElementById("time").html("["+strDate+"]");
        return  s;
    }
    //Function to generate unique data
    static String stringGenerator() {

        Random random = new Random();
        var h = new Double [15];
        String s = "";
        for (int i = 0; i <h.length ; i++) {
            h[i] = Math.floor(Math.random() * 10);
            s += h[i] + "   ";
        }
        s += " This would be so"+ "\n" + s + " mething like net"+ "\n"+s + " work traffic for"+ "\n"+s +"logging purpose";
        return s;
    }
    //Function to append trace to the trace file
    static Document finalBuilderHtml(Document file,Element el){
            file.body().children().last().appendChild(el);
            return file;

    }
}
